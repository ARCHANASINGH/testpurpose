package com.robosoft.archana.recyclerviewwithitemdecodemo.Widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

/**
 * Created by archana on 19/8/16.
 */
public class DividerItemDecoration extends RecyclerView.ItemDecoration {

    private static final int[] ATTRS = new int[]{
            android.R.attr.listDivider
    };

    public static final int HORIZONTAL_LIST = LinearLayoutManager.HORIZONTAL;

    public static final int VERTICAL_LIST = LinearLayoutManager.VERTICAL;

    private Drawable mDivider;

    private int mOrientation;
    private Context mContext;
    public DividerItemDecoration(Context context, int orientation) {
        this.mContext = context;
        Log.i("Hello","DividerItemDecoration*****"+"Oreinentation is"+orientation);
        final TypedArray a = context.obtainStyledAttributes(ATTRS);
        mDivider = a.getDrawable(0);
        a.recycle();
        setOrientation(orientation);
    }

    public void setOrientation(int orientation) {
        if (orientation != HORIZONTAL_LIST && orientation != VERTICAL_LIST) {
            throw new IllegalArgumentException("invalid orientation");
        }
        mOrientation = orientation;
    }

    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
        Log.i("Hello","onDraw()**********************************"+"Parent is"+parent);
        Log.i("Hello","TotalCHild onDraw()"+parent.getChildCount());
        if (mOrientation == VERTICAL_LIST) {
            drawVertical(c, parent);
        } else {
            drawHorizontal(c, parent);
        }
    }

    public void drawVertical(Canvas c, RecyclerView parent) {

        Log.i("Hello","drawVertical*******"+"Parent is"+parent);
        final int left = parent.getPaddingLeft();
        final int right = parent.getWidth() - parent.getPaddingRight();
        final int childCount = parent.getChildCount();// total no of visible item on screen
      //  Log.i("Hello","No of Child"+childCount);
        for (int i = 0; i < childCount; i++) {
            final View child = parent.getChildAt(i);
            final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child
                    .getLayoutParams();
            final int top = child.getBottom() + params.bottomMargin;
          //  Log.i("Hello","Top is"+top+"Bottom Margin is"+params.bottomMargin);
            final int bottom = top + mDivider.getIntrinsicHeight();
           // Log.i("Hello","Bottom is"+bottom+"IntrinsicHeight is"+mDivider.getIntrinsicHeight());
            mDivider.setBounds(left, top, right, bottom);
            mDivider.draw(c);
        }
    }

    public void drawHorizontal(Canvas c, RecyclerView parent) {
        Log.i("Hello","drawHorizental**********"+"Parent is"+parent);
        final int top = parent.getPaddingTop();
        final int bottom = parent.getHeight() - parent.getPaddingBottom();

        final int childCount = parent.getChildCount();//total no of visible item on screen
        Log.i("Hello","No of child"+childCount);
        for (int i = 0; i < childCount; i++) {
            if(i%2==0) {
                final View child = parent.getChildAt(i);
                final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child
                        .getLayoutParams();
                Log.i("Hello", "child.getRight()" + child.getRight() + "param.rightMargin" + params.rightMargin);
                final int left = child.getRight() + params.rightMargin;
                final int right = left + mDivider.getIntrinsicHeight();
                mDivider.setBounds(left, top, right, bottom);
                mDivider.draw(c);
            }
        }
    }

   // Notes
    // getItemOffsets() is being called as much number of visible view of recyclerView on screen.
    // i.e its depend on the no. of visible view of recyclerView on screen
  // todo analysis
   @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        Log.i("Hello","getItemOffsets()************************");
        super.getItemOffsets(outRect,view,parent,state);
        if (mOrientation == VERTICAL_LIST) {
           // Log.i("Hello","IntrinsicHeight*******"+mDivider.getIntrinsicHeight());
            outRect.set(0, 0, 0, mDivider.getIntrinsicHeight());
      }else {

            //int pos = parent.getChildAdapterPosition(view);

              //  Log.i("Hello", "IntrinsicWidth********" + mDivider.getIntrinsicWidth());
                outRect.set(0, 0, mDivider.getIntrinsicWidth(), 0);

        }
    }
}
