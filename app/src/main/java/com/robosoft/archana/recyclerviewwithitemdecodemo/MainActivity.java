package com.robosoft.archana.recyclerviewwithitemdecodemo;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.robosoft.archana.recyclerviewwithitemdecodemo.Widget.DividerItemDecoration;
import com.robosoft.archana.recyclerviewwithitemdecodemo.adapter.OrderAdapter;

public class MainActivity extends AppCompatActivity {

    private RecyclerView mOrderListRecyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initUi();
        setAdapter();
    }

    private void initUi(){
        ActionBar actionBar = getSupportActionBar();
        if(actionBar!=null){
           actionBar.setHomeButtonEnabled(true);
            actionBar.setTitle(getString(R.string.toolbartitle));
        }
        mOrderListRecyclerView = (RecyclerView) findViewById(R.id.order_recycle);
    }

    private void setAdapter(){
       GridLayoutManager linearLayoutManager = new GridLayoutManager(this,2, LinearLayoutManager.VERTICAL,false);
      //  LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        OrderAdapter orderAdapter = new OrderAdapter(this);
        mOrderListRecyclerView.setLayoutManager(linearLayoutManager);
        mOrderListRecyclerView.setAdapter(orderAdapter);

        Log.i("Hello","Total No of Child is"+mOrderListRecyclerView.getChildCount());
        mOrderListRecyclerView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL_LIST));
        mOrderListRecyclerView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.HORIZONTAL_LIST));
    }

    // check
     // test purpose

}
