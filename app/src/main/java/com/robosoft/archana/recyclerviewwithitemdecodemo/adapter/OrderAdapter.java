package com.robosoft.archana.recyclerviewwithitemdecodemo.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.robosoft.archana.recyclerviewwithitemdecodemo.R;

/**
 * Created by archana on 19/8/16.
 */
public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.OrderViewHolder> {

    private Context mContext;

    public OrderAdapter(Context mContext) {

        this.mContext = mContext;
    }

    @Override
    public OrderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View oneRow = LayoutInflater.from(mContext).inflate(R.layout.order_row,parent,false);
        OrderViewHolder orderViewHolder = new OrderViewHolder(oneRow);
        return orderViewHolder;
    }

    @Override
    public void onBindViewHolder(OrderViewHolder holder, int position) {
        if(!TextUtils.isEmpty(mContext.getResources().getString(R.string.black))) {
           holder.mTxtOrderDelivery.setText(R.string.order_delivery);
        }
        holder.mImgOrderDelivery.setImageResource(R.drawable.order_delivery);
    }

    @Override
    public int getItemCount() {
        return 3;
    }

    class OrderViewHolder extends RecyclerView.ViewHolder{
        private TextView mTxtOrderDelivery;
        private ImageView mImgOrderDelivery;

        public OrderViewHolder(View itemView) {
            super(itemView);
            mTxtOrderDelivery = (TextView) itemView.findViewById(R.id.txt_order_delivery);
            mImgOrderDelivery = (ImageView)itemView.findViewById(R.id.img_order_delivery);

        }
    }
}
